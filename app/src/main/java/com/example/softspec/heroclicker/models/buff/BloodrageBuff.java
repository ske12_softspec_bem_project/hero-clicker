package com.example.softspec.heroclicker.models.buff;

/**
 * Created by Earth on 28/5/2559.
 */
public class BloodrageBuff implements Buff {

    private double bonusDamage;

    public BloodrageBuff(double bonusDamage) {
        this.bonusDamage = bonusDamage;
    }

    @Override
    public double affect(double damage) {
        return damage * (bonusDamage - 1);
    }
}
