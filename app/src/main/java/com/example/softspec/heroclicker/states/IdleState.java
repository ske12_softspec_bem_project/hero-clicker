package com.example.softspec.heroclicker.states;

import com.example.softspec.heroclicker.models.Game;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Earth on 30/5/2559.
 */
public class IdleState implements State {

    private TimerTask timerTask;
    private Timer timer;
    private boolean isRunning;

    @Override
    public void doAction(final Game game) {
        timerTask = new TimerTask() {
            @Override
            public void run() {
                game.startAttackWithWeapon();
                isRunning = true;
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 1000, 500);

        game.setIdle(true);
    }

    @Override
    public void stopAction() {
        if(isRunning) {
            timer.cancel();
        }
    }
}
