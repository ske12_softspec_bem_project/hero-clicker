package com.example.softspec.heroclicker.models.spell;

import android.os.AsyncTask;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.buff.Buff;
import com.example.softspec.heroclicker.models.buff.MortalStrikeBuff;
import com.example.softspec.heroclicker.models.calculator.DamageCalculator;

/**
 * Created by Earth on 28/5/2559.
 */
public class MortalStrike extends Spell {

    private static final String NAME = "Mortal Strike";
    private static final String DESCRIPTION ="Give the 15% chance to deal a massive damage for 5 seconds.";
    private static final double CHANCE_RATE = 0.15;
    private double bonusDamage = 2.0;
    private static final double INCREASING_BONUS_DAMAGE_PER_LEVEL = 0.1;
    private static final double DECREASING_COOLDOWN_PER_LEVEL = 1;
    private static final double DURATION = 5.0;
    private static final long RANDOM_TIME = 100;

    private Buff buff = new MortalStrikeBuff( getActualBonusDamage() );

    public MortalStrike(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(Player player, Enemy enemy) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            long startTime = System.currentTimeMillis();

            @Override
            protected Void doInBackground(Void... params) {
                while ((System.currentTimeMillis() - startTime) / 1000.0 <= DURATION) {
                    double random = Math.random();
                    if (random <= CHANCE_RATE) {
                        System.out.println("MORTAL STRIKE !!");
                        DamageCalculator.getInstance().addBuff(buff);
                    }

                    try {
                        Thread.sleep(RANDOM_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }
        }.execute();
    }

    public double getActualBonusDamage() {
        return bonusDamage + (INCREASING_BONUS_DAMAGE_PER_LEVEL * (getLevel() - 1));
    }

    public void setBonusDamage(double bonusDamage) {
        this.bonusDamage = bonusDamage;
    }

    @Override
    public double getCooldown() {
        return super.getCooldown() - (DECREASING_COOLDOWN_PER_LEVEL * (getLevel()-1));
    }
}
