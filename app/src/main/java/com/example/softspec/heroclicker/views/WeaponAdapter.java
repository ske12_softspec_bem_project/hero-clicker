package com.example.softspec.heroclicker.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Game;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.models.Weapon;

import java.util.List;

/**
 * Created by Earth on 27/5/2559.
 */
public class WeaponAdapter extends ArrayAdapter<Weapon> {

    private List<Integer> weaponPictures;
    private Game game;
    private int selectedPosition;

    public WeaponAdapter(Context context, int resource, List<Weapon> objects, List<Integer> weaponPictures, Game game) {
        super(context, resource, objects);
        this.weaponPictures = weaponPictures;
        this.game = game;
        selectedPosition = Storage.getInstance().getSelectedWeaponPosition();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.weapon_cell, null);
        }

        if(v != null) {
            ImageView img_weapon = (ImageView) v.findViewById(R.id.img_weapon);
            TextView tv_weapon_name = (TextView) v.findViewById(R.id.tv_weapon_name);
            TextView tv_damage = (TextView) v.findViewById(R.id.tv_damage);
            TextView tv_weapon_description = (TextView) v.findViewById(R.id.tv_weapon_description);
            final RadioButton radioButton = (RadioButton) v.findViewById(R.id.radioButton);

            Weapon weapon = getItem(position);
            img_weapon.setImageResource(weaponPictures.get(Storage.getInstance().getWeapons().indexOf(weapon)));
            tv_weapon_name.setText(weapon.getName());
            tv_damage.setText(String.format("(%d ~ %d)", (int)weapon.getMinDamage(), (int)weapon.getMaxDamage()));
            tv_weapon_description.setText(weapon.getDescription());

            if(selectedPosition == position) {
                radioButton.setChecked(true);
            }

            else radioButton.setChecked(false);

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(radioButton.isChecked()) {
                        selectedPosition = position;
                        game.getPlayer().setWeapon(Storage.getInstance().getWeapons().get(selectedPosition));
                        Storage.getInstance().setSelectedWeaponPosition(selectedPosition);
                    }

                    notifyDataSetChanged();
                }
            });
        }
        return v;
    }
}
