package com.example.softspec.heroclicker.models.spell;

import android.os.AsyncTask;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.buff.Buff;
import com.example.softspec.heroclicker.models.buff.FurySwipesBuff;
import com.example.softspec.heroclicker.models.calculator.DamageCalculator;

/**
 * Created by Earth on 28/5/2559.
 */
public class FurySwipes extends Spell {

    private static final String NAME = "Fury Swipes";
    private static final String DESCRIPTION ="Each attack on the enemy causes consecutive attacks to deal a bonus damage for 5 seconds.";
    private double bonusDamage = 40;
    private static final double INCREASING_BONUS_DAMAGE_PER_LEVEL = 20;
    private static final double DURATION = 5;

    private Buff buff = new FurySwipesBuff(getActualBonusDamage());

    public FurySwipes(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(Player player, Enemy enemy) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            long startTime = System.currentTimeMillis();
            @Override
            protected Void doInBackground(Void... params) {
                while((System.currentTimeMillis() - startTime) / 1000.0 <= DURATION) {
                    DamageCalculator.getInstance().addBuff(buff);
                }
                return null;
            }
        }.execute();
    }

    public double getActualBonusDamage() {
        return bonusDamage + (INCREASING_BONUS_DAMAGE_PER_LEVEL * (getLevel() - 1));
    }

    public void setBonusDamage(double bonusDamage) {
        this.bonusDamage = bonusDamage;
    }
}
