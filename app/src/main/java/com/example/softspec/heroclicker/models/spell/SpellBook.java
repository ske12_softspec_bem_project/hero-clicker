package com.example.softspec.heroclicker.models.spell;

/**
 * Created by Earth on 29/5/2559.
 */
public class SpellBook {
    private Spell[] spells;
    private static final int SPELL_BOOK_CAPACITY = 3;

    public SpellBook() {
        spells = new Spell[SPELL_BOOK_CAPACITY];
    }

    public void addSpell(Spell spell) {
        for (int i = 0; i < SPELL_BOOK_CAPACITY; i++) {
            if (spells[i] == null) {
                spells[i] = spell;
                return;
            }
        }
    }

    public void removeSpell(Spell spell) {
        for (int i = 0; i < SPELL_BOOK_CAPACITY; i++) {
            if (spell.equals(spells[i])) {
                spells[i] = null;
                return;
            }
        }
    }

    public void clearSpell() {
        for (int i = 0; i < SPELL_BOOK_CAPACITY; i++) {
            spells[i] = null;
        }
    }

    public boolean contains(Spell spell) {
        for (int i = 0; i < SPELL_BOOK_CAPACITY; i++) {
            if(spells[i] != null && spells[i].equals(spell)) {
                return true;
            }
        }

        return false;
    }

    public Spell[] getContent() {
        return spells;
    }
}
