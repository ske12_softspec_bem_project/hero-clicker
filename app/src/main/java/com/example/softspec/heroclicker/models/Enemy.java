package com.example.softspec.heroclicker.models;

/**
 * Created by Earth on 28/5/2559.
 */
public class Enemy {
    private String name;
    private double currentHP;
    private double HP;
    private String color;

    public static class Builder {

        private String name = "Demo";
        private double HP;
        private String color = "Original";


        public Builder(String name) {
            this.name = name;
        }

        public Builder HP(double HP){
            this.HP = HP;
            return this;
        }

        public Builder color(String color){
            this.color = color;
            return this;
        }

        public Enemy build() {
            return new Enemy(this);
        }

    }

    public Enemy(Builder builder) {
        this.name = builder.name;
        this.HP = builder.HP;
        this.currentHP = builder.HP;
        this.color = builder.color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(double currentHP) {
        this.currentHP = currentHP;
    }

    public double getHP() {
        return HP;
    }

    public void setHP(double HP) {
        this.HP = HP;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if(!(o instanceof Enemy)) return false;

        Enemy enemy = (Enemy) o;

        return this.getName().equalsIgnoreCase(enemy.getName()) && this.getHP() == enemy.getCurrentHP() && this.getColor().equalsIgnoreCase(enemy.getColor());
    }

    @Override
    public String toString() {
        return String.format("%s %s", getColor(), getName());
    }
}
