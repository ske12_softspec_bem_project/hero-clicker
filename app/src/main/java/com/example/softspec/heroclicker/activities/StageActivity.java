package com.example.softspec.heroclicker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Stage;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.views.StageAdapter;

import java.util.ArrayList;
import java.util.List;

public class StageActivity extends AppCompatActivity {

    private List<Stage> stages;
    private StageAdapter stageAdapter;
    private ListView stageListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initComponents();
    }

    protected void initComponents() {
        stages = new ArrayList<Stage>();
        stages = Storage.getInstance().getStages();
        stageAdapter = new StageAdapter(this, R.layout.stage_cell, stages);
        stageListView = (ListView) findViewById(R.id.list_view);
        stageListView.setAdapter(stageAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        else if(id == android.R.id.home) {
            finish();
            return true;
        }

        else if(id == R.id.action_inventory) {
            Intent intent = new Intent(StageActivity.this, InventoryActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}

