package com.example.softspec.heroclicker.models;

import com.example.softspec.heroclicker.managers.CooldownManager;
import com.example.softspec.heroclicker.managers.CriteriaManager;
import com.example.softspec.heroclicker.managers.EnemyManager;
import com.example.softspec.heroclicker.models.command.SpellCommand;
import com.example.softspec.heroclicker.models.command.WeaponCommand;
import com.example.softspec.heroclicker.models.spell.Spell;
import com.example.softspec.heroclicker.models.spell.SpellBook;
import com.example.softspec.heroclicker.states.State;

import java.util.Observable;

/**
 * Created by Earth on 28/5/2559.
 */
public class Game extends Observable {
    private Player player;
    private Enemy enemy;

    private SpellBook spellBook;

    private WeaponCommand weaponCommand;
    private SpellCommand spellCommand;

    private EnemyManager enemyManager;
    private CooldownManager cooldownManager;
    private CriteriaManager criteriaManager;

    private State state;

    private boolean isIdle;

    public Game(Player player, Enemy enemy) {
        this.player = player;
        this.enemy = enemy;

        spellBook = new SpellBook();

        weaponCommand = new WeaponCommand();
        spellCommand = new SpellCommand(null);

        isIdle = false;

        enemyManager = new EnemyManager(enemy);
        cooldownManager = new CooldownManager();
        criteriaManager = new CriteriaManager();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public WeaponCommand getWeaponCommand() {
        return weaponCommand;
    }

    public void setWeaponCommand(WeaponCommand weaponCommand) {
        this.weaponCommand = weaponCommand;
    }

    public SpellCommand getSpellCommand() {
        return spellCommand;
    }

    public void setSpellCommand(SpellCommand spellCommand) {
        this.spellCommand = spellCommand;
    }

    public SpellBook getSpellBook() {
        return spellBook;
    }

    public void setSpellBook(SpellBook spellBook) {
        this.spellBook = spellBook;
        setChanged();
        notifyObservers(spellBook);
    }

    public void startAttackWithWeapon() {
        weaponCommand.execute(player, enemy);
    }

    public void startAttackWithSpell(Spell spell) {
        spellCommand.setSpell(spell);
        spellCommand.execute(player, enemy);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isIdle() {
        return isIdle;
    }

    public void setIdle(boolean idle) {
        isIdle = idle;
        setChanged();
        notifyObservers(isIdle);
    }

    public EnemyManager getEnemyManager() {
        return enemyManager;
    }

    public void setEnemyManager(EnemyManager enemyManager) {
        this.enemyManager = enemyManager;
    }

    public CooldownManager getCooldownManager() {
        return cooldownManager;
    }

    public void setCooldownManager(CooldownManager cooldownManager) {
        this.cooldownManager = cooldownManager;
    }

    public CriteriaManager getCriteriaManager() {
        return criteriaManager;
    }

    public void setCriteriaManager(CriteriaManager criteriaManager) {
        this.criteriaManager = criteriaManager;
    }
}
