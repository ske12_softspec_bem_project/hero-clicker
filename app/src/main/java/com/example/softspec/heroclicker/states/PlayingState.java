package com.example.softspec.heroclicker.states;

import com.example.softspec.heroclicker.models.Game;

/**
 * Created by Earth on 30/5/2559.
 */
public class PlayingState implements State {

    @Override
    public void doAction(Game game) {
        game.setIdle(false);
    }

    @Override
    public void stopAction() {

    }
}
