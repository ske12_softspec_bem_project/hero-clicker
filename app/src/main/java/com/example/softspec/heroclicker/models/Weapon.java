package com.example.softspec.heroclicker.models;

/**
 * Created by Earth on 27/5/2559.
 */
public class Weapon {
    private String name;
    private String description;
    private double minDamage;
    private double maxDamage;
    private double bonusDamagePerLevel;
    private int level;

    public Weapon(String name, String description, double minDamage, double maxDamage, double bonusDamagePerLevel, int level) {
        this.name = name;
        this.description = description;
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        this.bonusDamagePerLevel = bonusDamagePerLevel;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getMinDamage() {
        return minDamage + bonusDamagePerLevel * (level-1);
    }

    public void setMinDamage(double minDamage) {
        this.minDamage = minDamage;
    }

    public double getMaxDamage() {
        return maxDamage + bonusDamagePerLevel * (level-1);
    }

    public void setMaxDamage(double maxDamage) {
        this.maxDamage = maxDamage;
    }

    public double getBonusDamagePerLevel() {
        return bonusDamagePerLevel;
    }

    public void setBonusDamagePerLevel(double bonusDamagePerLevel) {
        this.bonusDamagePerLevel = bonusDamagePerLevel;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getActualDamage() {
        return getMinDamage() + (Math.random() * (getMaxDamage() - getMinDamage()));
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if(!(o instanceof Weapon)) return false;

        Weapon weapon = (Weapon) o;
        return this.getName().equalsIgnoreCase(weapon.getName());
    }

    @Override
    public String toString() {
        return getName();
    }
}
