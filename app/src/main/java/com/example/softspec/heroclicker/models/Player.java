package com.example.softspec.heroclicker.models;

/**
 * Created by Earth on 28/5/2559.
 */
public class Player {
    private String name;
    private Weapon weapon;

    public Player(String name, Weapon weapon) {
        this.name = name;
        this.weapon = weapon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
