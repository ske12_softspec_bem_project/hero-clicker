package com.example.softspec.heroclicker.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.views.WeaponInventoryAdapter;

public class WeaponInventoryFragment extends Fragment {

    private ListView listView;
    private WeaponInventoryAdapter weaponInventoryAdapter;

    public WeaponInventoryFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weapon_inventory, container, false);
        listView = (ListView) view.findViewById(R.id.list_view);
        weaponInventoryAdapter = new WeaponInventoryAdapter(this.getContext(), R.layout.weapon_inventory_cell, Storage.getInstance().getWeapons(), Storage.getInstance().getWeaponPictures());
        listView.setAdapter(weaponInventoryAdapter);
        return view;
    }
}
