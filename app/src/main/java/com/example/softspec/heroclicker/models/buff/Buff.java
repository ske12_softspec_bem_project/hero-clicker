package com.example.softspec.heroclicker.models.buff;

/**
 * Created by Earth on 28/5/2559.
 */
public interface Buff {
    double affect(double damage);
}
