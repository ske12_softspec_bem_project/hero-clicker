package com.example.softspec.heroclicker.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Game;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.models.spell.Spell;
import com.example.softspec.heroclicker.models.spell.SpellBook;

import java.util.List;

/**
 * Created by Earth on 28/5/2559.
 */
public class SpellAdapter extends ArrayAdapter<Spell> {

    private List<Integer> spellPictures;
    private Game game;
    private SpellBook spellBook;

    public SpellAdapter(Context context, int resource, List<Spell> objects, List<Integer> spellPictures, Game game) {
        super(context, resource, objects);
        this.spellPictures = spellPictures;
        this.game = game;
        this.spellBook = game.getSpellBook();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.spell_cell, null);
        }

        if (v != null) {
            ImageView img_spell = (ImageView) v.findViewById(R.id.img_spell);
            TextView tv_spell_name = (TextView) v.findViewById(R.id.tv_spell_name);
            TextView tv_spell_description = (TextView) v.findViewById(R.id.tv_spell_description);
            TextView tv_level = (TextView) v.findViewById(R.id.tv_level);
            final CheckBox checkBox = (CheckBox) v.findViewById(R.id.check_box);

            final Spell spell = getItem(position);
            img_spell.setImageResource(spellPictures.get(Storage.getInstance().getSpells().indexOf(spell)));
            tv_spell_name.setText(spell.getName());
            tv_level.setText(String.format("(Level %d)", spell.getLevel()));
            tv_spell_description.setText(spell.getDescription());

            if(spellBook.contains(spell)) {
                checkBox.setChecked(true);
            }

            else checkBox.setChecked(false);

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(checkBox.isChecked()) {
                        spellBook.addSpell(spell);
                    }

                    else {
                        spellBook.removeSpell(spell);
                    }

                    game.setSpellBook(spellBook);
                    notifyDataSetChanged();
                }
            });
        }
        return v;

    }
}
