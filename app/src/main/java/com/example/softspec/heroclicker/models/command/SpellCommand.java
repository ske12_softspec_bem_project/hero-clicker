package com.example.softspec.heroclicker.models.command;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.spell.Spell;

import java.util.Observable;

/**
 * Created by Earth on 28/5/2559.
 */
public class SpellCommand extends Observable implements Command {

    private Spell spell;

    public SpellCommand(Spell spell) {
        this.spell = spell;
    }

    @Override
    public void execute(Player player, Enemy enemy) {
        spell.apply(player, enemy);
        setChanged();
        notifyObservers();
    }


    public Spell getSpell() {
        return spell;
    }

    public void setSpell(Spell spell) {
        this.spell = spell;
    }
}
