package com.example.softspec.heroclicker.managers;

import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.models.spell.Spell;
import com.example.softspec.heroclicker.models.timer.CooldownTimer;

/**
 * Created by Earth on 31/5/2559.
 */
public class CooldownManager {
    private CooldownTimer cooldownTimer;


    public CooldownManager() {
        cooldownTimer = new CooldownTimer();
    }

    public void cast(Spell spell) {
        spell.setOnCooldown(true);
        int index = Storage.getInstance().getSpells().indexOf(spell);
        cooldownTimer.start(index);
    }

    public CooldownTimer getCooldownTimer() {
        return cooldownTimer;
    }
}
