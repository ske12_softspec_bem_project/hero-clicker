package com.example.softspec.heroclicker.models.command;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;

/**
 * Created by Earth on 28/5/2559.
 */
public interface Command {
    void execute(Player player, Enemy enemy);

}
