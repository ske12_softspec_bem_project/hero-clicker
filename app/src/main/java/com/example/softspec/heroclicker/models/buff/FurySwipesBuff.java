package com.example.softspec.heroclicker.models.buff;

/**
 * Created by Earth on 28/5/2559.
 */
public class FurySwipesBuff implements Buff {

    private double bonusDamage;

    public FurySwipesBuff(double bonusDamage) {
        this.bonusDamage = bonusDamage;
    }

    @Override
    public double affect(double damage) {
        return bonusDamage;
    }
}
