package com.example.softspec.heroclicker.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.views.SpellInventoryAdapter;

public class SpellInventoryFragment extends Fragment {

    private ListView listView;

    public SpellInventoryFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spell_inventory, container, false);
        listView = (ListView) view.findViewById(R.id.list_view);
        SpellInventoryAdapter spellInventoryAdapter = new SpellInventoryAdapter(this.getContext(), R.layout.spell_inventory_cell, Storage.getInstance().getSpells(), Storage.getInstance().getSpellPictures());
        listView.setAdapter(spellInventoryAdapter);
        return view;
    }
}
