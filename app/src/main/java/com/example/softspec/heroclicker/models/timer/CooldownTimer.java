package com.example.softspec.heroclicker.models.timer;

import android.os.CountDownTimer;

import com.example.softspec.heroclicker.models.Storage;

import java.util.Observable;

/**
 * Created by Earth on 30/5/2559.
 */
public class CooldownTimer extends Observable {
    private CountDownTimer[] countDownTimers;

    public CooldownTimer() {
        countDownTimers = new CountDownTimer[Storage.getInstance().getSpells().size()];

        for(int i = 0; i < countDownTimers.length; i++) {
            final int index = i;
            countDownTimers[i] = new CountDownTimer((long) Storage.getInstance().getSpells().get(index).getCooldown() * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    System.out.println(Storage.getInstance().getSpells().get(index).getName() + " cooldown remaining: " + millisUntilFinished / 1000.0);
                }

                @Override
                public void onFinish() {
                    Storage.getInstance().getSpells().get(index).setOnCooldown(false);
                    setChanged();
                    notifyObservers(Storage.getInstance().getSpells().get(index));
                    System.out.println(Storage.getInstance().getSpells().get(index).getName() + " Ready");
                }
            };
        }
    }

    public void start(int index) {
        countDownTimers[index].start();
    }
}
