package com.example.softspec.heroclicker.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Game;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.Stage;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.models.spell.Spell;
import com.example.softspec.heroclicker.models.spell.SpellBook;
import com.example.softspec.heroclicker.states.IdleState;
import com.example.softspec.heroclicker.states.PlayingState;
import com.example.softspec.heroclicker.states.State;
import com.example.softspec.heroclicker.views.SpellAdapter;
import com.example.softspec.heroclicker.views.WeaponAdapter;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {

    private BottomBar bottomBar;
    private WeaponAdapter weaponAdapter;
    private SpellAdapter spellAdapter;
    private ListView listView;
    private ImageView img_enemy;
    private TextView tv_coin;
    private TextView tv_enemy_name;
    private ProgressBar progressBar;

    private LinearLayout layout_img_spells;
    private ImageView img_spell_1;
    private ImageView img_spell_2;
    private ImageView img_spell_3;

    private ImageView[] img_spells;

    private int[] img_enemy_state_image_id;

    private Player player;
    private Enemy enemy;
    private Game game;

    private State playingState;
    private State idleState;

    private Map<ImageView, Spell> viewSpellMap;

    private TextView tv_damage_point;

    private ObjectAnimator mover, fadeOut;

    private static boolean isOpened = false;

    private float recentY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initGame();
        initComponents();
        initBottomBar(savedInstanceState);

    }

    private void initGame() {
        player = Storage.getInstance().getPlayer();
        enemy = Storage.getInstance().getEnemyFromQuery(getIntent().getStringExtra("Enemy"));
        enemy.setCurrentHP(enemy.getHP());

        game = new Game(player, enemy);

        playingState = new PlayingState();
        idleState = new IdleState();

        game.setState(playingState);

        game.addObserver(this);
        game.getWeaponCommand().addObserver(this);
        game.getSpellCommand().addObserver(this);
        game.getCooldownManager().getCooldownTimer().addObserver(this);

        for(Spell spell : Storage.getInstance().getSpells()) {
            spell.addObserver(this);
        }
    }

    private void initComponents() {
        listView = (ListView) findViewById(R.id.list_view);
        weaponAdapter = new WeaponAdapter(this, R.layout.weapon_cell, Storage.getInstance().getAvailableWeapons(), Storage.getInstance().getWeaponPictures(), game);
        spellAdapter = new SpellAdapter(this, R.layout.spell_cell, Storage.getInstance().getAvailableSpells(), Storage.getInstance().getSpellPictures(), game);

        tv_enemy_name = (TextView) findViewById(R.id.tv_enemy_name);
        tv_enemy_name.setText(enemy.toString());

        tv_coin = (TextView) findViewById(R.id.tv_coin);
        tv_coin.setText("COIN: " + Storage.getInstance().getCoin());

        tv_damage_point = (TextView) findViewById(R.id.tv_damage_point);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setProgress(100);

        img_enemy_state_image_id = new int[4];
        img_enemy = (ImageView) findViewById(R.id.img_enemy);

        String[] img_enemy_state_resources = Storage.getInstance().getEnemyStateImageResources(enemy);

        for(int i = 0; i < img_enemy_state_image_id.length; i++) {
            img_enemy_state_image_id[i] = img_enemy.getContext().getResources().getIdentifier(img_enemy_state_resources[i], "drawable", img_enemy.getContext().getPackageName());
        }

        img_enemy.setImageResource(img_enemy_state_image_id[0]);

        img_enemy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.startAttackWithWeapon();
            }
        });

        layout_img_spells = (LinearLayout) findViewById(R.id.layout_img_spells);

        img_spell_1 = (ImageView) findViewById(R.id.img_spell_1);
        img_spell_2 = (ImageView) findViewById(R.id.img_spell_2);
        img_spell_3 = (ImageView) findViewById(R.id.img_spell_3);

        img_spells = new ImageView[]{img_spell_1, img_spell_2, img_spell_3};

        viewSpellMap = new HashMap<>(3);
        viewSpellMap.put(img_spells[0], null);
        viewSpellMap.put(img_spells[1], null);
        viewSpellMap.put(img_spells[2], null);

        for (int i = 0; i < img_spells.length; i++) {
            final int index = i;
            img_spells[index].setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Spell spell = viewSpellMap.get(img_spells[index]);

                    if(spell == null) return;

                    setSpellButtonEnabled(img_spells[index], false);

                    game.startAttackWithSpell(spell);
                    game.getCooldownManager().cast(spell);
                }
            });
        }

        mover = ObjectAnimator.ofFloat(tv_damage_point, "y", tv_damage_point.getY(), tv_damage_point.getY() - 20);
        mover.setDuration(200);

        fadeOut = ObjectAnimator.ofFloat(tv_damage_point, "alpha", 1f, 0f);
        fadeOut.setDuration(500);

        recentY = tv_damage_point.getY();
    }

    private void initBottomBar(Bundle savedInstanceState) {
        bottomBar = BottomBar.attach(this, savedInstanceState);
        bottomBar.setItemsFromMenu(R.menu.menu_bottom, new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {

                if (menuItemId == R.id.weapon){
                    listView.setAdapter(weaponAdapter);
                }
                else if (menuItemId == R.id.spell){
                    listView.setAdapter(spellAdapter);
                }
                else if(menuItemId == R.id.monster){

                }
                else if (menuItemId == R.id.stats){

                }

                if(!isOpened) {
                    showMenu();
                }

            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if(isOpened) {
                    hideMenu();
                }
                else {
                   showMenu();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        else if(id == android.R.id.home) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Quit");
            builder.setMessage("Would you like to give up ?");
            builder.setPositiveButton("Give up", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent intent = new Intent(MainActivity.this, StageActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            builder.setNegativeButton("Keep Fighting", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();

        }

        else if(id == R.id.action_state) {
            if(game.getState() == playingState) {
                game.setState(idleState);
                idleState.doAction(game);
                item.setIcon(R.drawable.ic_playing);
            }

            else {
                game.setState(playingState);
                playingState.doAction(game);
                idleState.stopAction();
                item.setIcon(R.drawable.ic_idle);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update(Observable observable, Object data) {
        if(data instanceof SpellBook || data instanceof Spell || data instanceof Boolean) {
            updateSpellButtons();
        }

        if(data instanceof Boolean) {
            boolean isIdle = (boolean) data;

            if(!isIdle) {
                img_enemy.setEnabled(true);
                layout_img_spells.setEnabled(true);
            }
            else {
                img_enemy.setEnabled(false);
                layout_img_spells.setEnabled(false);
            }
        }

        else if(data instanceof Double) {
            final double damage = (double) data;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateProgressBar();
                    updateCoin();
                    img_enemy.setImageResource(img_enemy_state_image_id[game.getEnemyManager().getEnemyStateImageIndex()]);
                    tv_damage_point.setVisibility(View.VISIBLE);
                    tv_damage_point.setAlpha(1f);

                    if (damage > 0){
                        tv_damage_point.setTextColor(Color.parseColor("#FF000000"));
                        tv_damage_point.setText( "" + Math.round(damage));
                    } else if (damage == 0) {
                        tv_damage_point.setText( "Miss" );
                        tv_damage_point.setTextColor(Color.parseColor("#FF0000"));
                    }

                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.play(mover).before(fadeOut);
                    animatorSet.start();
                    tv_damage_point.setY(recentY);
                }
            });
        }

        else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateProgressBar();
                    updateCoin();
                    img_enemy.setImageResource(img_enemy_state_image_id[game.getEnemyManager().getEnemyStateImageIndex()]);
                }
            });
        }
    }

    private void setSpellButtonEnabled(ImageView img_spell, boolean enabled) {
        if(enabled) {
            img_spell.setEnabled(true);
            img_spell.setAlpha(1f);
        }

        else {
            img_spell.setEnabled(false);
            img_spell.setAlpha(0.3f);
        }
    }

    private void updateSpellButtons() {
        Spell[] spells = game.getSpellBook().getContent();
        for(int i = 0; i < spells.length; i++) {
            img_spells[i].setImageResource(Storage.getInstance().getSpellContentPicture(spells[i]));
            viewSpellMap.put(img_spells[i], spells[i]);

            if(spells[i] == null || (spells[i] != null && !spells[i].isOnCooldown() && !game.isIdle())) {
                setSpellButtonEnabled(img_spells[i], true);
            }

            else {
                setSpellButtonEnabled(img_spells[i], false);
            }
        }
    }

    private void updateProgressBar() {
        game.getCriteriaManager().start();
        int enemyPercentageHP = (int) (enemy.getCurrentHP() * 100 / enemy.getHP());
        progressBar.setProgress(enemyPercentageHP);
        if(enemyPercentageHP <= 0) {
            end();
        }
    }

    private void end() {
        idleState.stopAction();
        LayoutInflater inflater = LayoutInflater.from(this);
        View alertDialogView = inflater.inflate(R.layout.dialog_rating, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setView(alertDialogView);

        RatingBar ratingBar = (RatingBar) alertDialogView.findViewById(R.id.rating_bar);
        Button btn_done = (Button) alertDialogView.findViewById(R.id.btn_done);

        int stage_position = getIntent().getIntExtra("stage_position", 0);
        float rating = game.getCriteriaManager().getRating();
        ratingBar.setRating(rating);
        game.getCriteriaManager().saveRatings(stage_position, rating);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(MainActivity.this, StageActivity.class);
                startActivity(intent);
                finish();
            }
        });

        if(!MainActivity.this.isFinishing()) {
            alertDialog.show();
        }
    }

    private void updateCoin() {
        tv_coin.setText("COIN: " + Storage.getInstance().getCoin());
    }

    private void showMenu() {
        listView.setVisibility(View.VISIBLE);
        layout_img_spells.setVisibility(View.GONE);
        isOpened = true;
    }

    private void hideMenu() {
        listView.setVisibility(View.GONE);
        layout_img_spells.setVisibility(View.VISIBLE);
        isOpened = false;
    }
}
