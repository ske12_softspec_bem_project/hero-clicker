package com.example.softspec.heroclicker.models.calculator;

import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.buff.Buff;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Earth on 28/5/2559.
 */
public class DamageCalculator {
    private static DamageCalculator instance = null;
    private Set<Buff> buffs;
    private static final double CONSTANT_MISS_RATE = 0.15;
    private double missRate = CONSTANT_MISS_RATE;

    private DamageCalculator() {
        buffs = new HashSet<>();
    }

    public static DamageCalculator getInstance() {
        if(instance == null) {
            instance = new DamageCalculator();
        }

        return instance;
    }

    public double calculate(Player player) {
        double damage = player.getWeapon().getActualDamage();
        for(Buff buff : buffs) {
            damage += buff.affect(damage);
        }
        clearBuff();

        if(Math.random() <= missRate) {
            damage = 0;
            System.out.println("MISS");
        }

        return damage;
    }

    public void addBuff(Buff buff) {
        buffs.add(buff);
    }

    public void removeBuff(Buff buff) {
        buffs.remove(buff);
    }

    public void clearBuff() {
        buffs.clear();
    }

    public double getMissRate() {
        return missRate;
    }

    public void setMissRate(double missRate) {
        this.missRate = missRate;
    }

    public static double getConstantMissRate() {
        return CONSTANT_MISS_RATE;
    }
}
