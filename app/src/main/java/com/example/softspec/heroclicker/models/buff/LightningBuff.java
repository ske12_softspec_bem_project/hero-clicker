package com.example.softspec.heroclicker.models.buff;

/**
 * Created by Earth on 30/5/2559.
 */
public class LightningBuff implements Buff {

    private double bonusDamage;

    public LightningBuff(double bonusDamage) {
        this.bonusDamage = bonusDamage;
    }

    @Override
    public double affect(double damage) {
        return bonusDamage;
    }
}
