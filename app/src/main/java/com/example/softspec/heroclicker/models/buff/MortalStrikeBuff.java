package com.example.softspec.heroclicker.models.buff;

/**
 * Created by Earth on 28/5/2559.
 */
public class MortalStrikeBuff implements Buff {

    private double bonusDamage;

    public MortalStrikeBuff(double bonusDamage) {
        this.bonusDamage = bonusDamage;
    }

    @Override
    public double affect(double damage) {
        System.out.println("Mortal Strike !!");
        return damage * (bonusDamage-1);
    }
}
