package com.example.softspec.heroclicker.activities;

import android.app.Application;
import android.content.Context;

/**
 * Created by Earth on 1/6/2559.
 */
public class StorageApplication extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
