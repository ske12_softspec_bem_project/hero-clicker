package com.example.softspec.heroclicker.managers;

import com.example.softspec.heroclicker.models.Enemy;

/**
 * Created by Earth on 31/5/2559.
 */
public class EnemyManager {
    private Enemy enemy;

    public EnemyManager(Enemy enemy) {
        this.enemy = enemy;
    }

    public int getEnemyStateImageIndex() {
        if (enemy.getHP() * 0.75 <= enemy.getCurrentHP()) {
            return 0;
        } else if (enemy.getHP() * 0.5 <= enemy.getCurrentHP()) {
            return 1;
        } else if (enemy.getHP() * 0.25 <= enemy.getCurrentHP()) {
            return 2;
        } else {
            return 3;
        }
    }
}
