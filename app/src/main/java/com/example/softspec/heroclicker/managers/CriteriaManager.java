package com.example.softspec.heroclicker.managers;

import com.example.softspec.heroclicker.models.Stage;
import com.example.softspec.heroclicker.models.Storage;

/**
 * Created by Earth on 1/6/2559.
 */
public class CriteriaManager {

    long startTime = 0;
    boolean isRunning = false;

    public void start() {
        if(!isRunning) {
            isRunning = true;
            startTime = System.currentTimeMillis();
        }
    }

    public float getRating() {
        long currentTime = System.currentTimeMillis();
        double timeElapsed = (currentTime - startTime) / 1000.0;

        if(timeElapsed <= 30) {
            return 3f;
        }

        else if(timeElapsed <= 45) {
            return 2f;
        }

        else {
            return 1f;
        }
    }

    public void saveRatings(int stage_position, float rating) {
        Stage stage = Storage.getInstance().getStages().get(stage_position);
        if(stage.getRating() > rating) return;
        stage.setRating(rating);
        Storage.getInstance().saveStageRatings(Storage.getInstance().getStageRatings(), "stageRatings");
    }

    public boolean isRunning() {
        return isRunning;
    }
}
