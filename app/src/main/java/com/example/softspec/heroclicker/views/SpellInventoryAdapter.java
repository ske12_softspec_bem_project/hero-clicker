package com.example.softspec.heroclicker.views;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.models.Weapon;
import com.example.softspec.heroclicker.models.spell.Spell;

import java.util.List;

/**
 * Created by Earth on 1/6/2559.
 */
public class SpellInventoryAdapter extends ArrayAdapter<Spell> {

    private List<Integer> spellPictures;

    public SpellInventoryAdapter(Context context, int resource, List<Spell> objects, List<Integer> spellPictures) {
        super(context, resource, objects);
        this.spellPictures = spellPictures;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.spell_inventory_cell, null);
        }

        if(v != null) {
            ImageView img_spell = (ImageView) v.findViewById(R.id.img_spell);
            TextView tv_spell_name = (TextView) v.findViewById(R.id.tv_spell_name);
            TextView tv_spell_description = (TextView) v.findViewById(R.id.tv_spell_description);
            TextView tv_level = (TextView) v.findViewById(R.id.tv_level);
            final ImageView img_upgrade = (ImageView) v.findViewById(R.id.img_upgrade);

            final Spell spell = getItem(position);
            img_spell.setImageResource(spellPictures.get(position));
            tv_spell_name.setText(spell.getName());
            tv_level.setText(String.format("(Level %d)", spell.getLevel()));
            tv_spell_description.setText(spell.getDescription());

            img_upgrade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(spell.getLevel() < 5) {
                        LayoutInflater inflater = LayoutInflater.from(getContext());
                        final View alertDialogView = inflater.inflate(R.layout.dialog_spell_upgrade, null);
                        final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();

                        alertDialog.setView(alertDialogView);

                        TextView tv_spell_detail = (TextView) alertDialogView.findViewById(R.id.tv_spell_detail);
                        TextView tv_player_coin = (TextView) alertDialogView.findViewById(R.id.tv_player_coin);
                        TextView tv_cost_of_upgrade = (TextView) alertDialogView.findViewById(R.id.tv_cost_of_upgrade);
                        TextView tv_player_coin_remaining = (TextView) alertDialogView.findViewById(R.id.tv_player_coin_remaining);

                        Button btn_upgrade = (Button) alertDialogView.findViewById(R.id.btn_upgrade);
                        Button btn_cancel = (Button) alertDialogView.findViewById(R.id.btn_cancel);

                        tv_spell_detail.setText(String.format("%s (Level %d -> %d)", spell.getName(), spell.getLevel(), spell.getLevel() + 1));

                        final long player_coin = Storage.getInstance().getCoin();
                        final long cost_of_upgrade = Storage.getInstance().getSpellUpgradeCost(spell);
                        final long player_coin_remaining = player_coin - cost_of_upgrade;

                        if(player_coin_remaining < 0) {
                            btn_upgrade.setEnabled(false);
                        }

                        tv_player_coin.setText(player_coin + "");
                        tv_cost_of_upgrade.setText(cost_of_upgrade + "");
                        tv_player_coin_remaining.setText(player_coin_remaining + "");

                        btn_upgrade.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                spell.setLevel(spell.getLevel() + 1);
                                Storage.getInstance().saveLevel(Storage.getInstance().getSpellLevel(), "spellLevel");
                                Storage.getInstance().setCoin(player_coin_remaining);
                                if(spell.getLevel() == 1) {
                                    Storage.getInstance().getAvailableSpells().add(spell);
                                }
                                alertDialog.dismiss();
                                notifyDataSetChanged();
                            }
                        });

                        btn_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });

                        alertDialog.show();
                    }

                    else {
                        img_upgrade.setEnabled(false);
                        img_upgrade.setAlpha(0.3f);
                    }

                    notifyDataSetChanged();
                }
            });
        }

        return v;
    }
}
