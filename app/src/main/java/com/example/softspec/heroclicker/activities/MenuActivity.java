package com.example.softspec.heroclicker.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.Storage;
import com.google.gson.Gson;

public class MenuActivity extends AppCompatActivity {

    private Button btn_play;
    private TextView tv_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initComponents();

    }

    protected void initComponents() {
        btn_play = (Button) findViewById(R.id.btn_play);
        tv_name = (TextView) findViewById(R.id.tv_player_name);

        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, StageActivity.class);
                startActivity(intent);
            }
        });

        LayoutInflater inflater = LayoutInflater.from(this);

        final View alertDialogView = inflater.inflate(R.layout.dialog_register, null);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setView(alertDialogView);

        final EditText edtxt_name = (EditText) alertDialogView.findViewById(R.id.edtxt_name);

        alertDialogView.findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtxt_name.getText().toString().length() == 0) {
                    edtxt_name.setError("Please fill the name.");
                }

                else {
                    Player player = new Player(edtxt_name.getText().toString(), Storage.getInstance().getWeaponFromQuery("Primal Axe"));
                    Storage.getInstance().setPlayer(player);
                    tv_name.setText("Welcome, " + player.getName());
                    alertDialog.dismiss();
                }
            }
        });

        if(Storage.getInstance().getPlayer() == null) {
            alertDialog.show();
        }

        else {
            tv_name.setText("Welcome, " + Storage.getInstance().getPlayer().getName());
        }
    }
}
