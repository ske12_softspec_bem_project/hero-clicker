package com.example.softspec.heroclicker.views;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.models.Weapon;

import java.util.List;

/**
 * Created by Earth on 31/5/2559.
 */
public class WeaponInventoryAdapter extends ArrayAdapter<Weapon> {

    private List<Integer> weaponPictures;

    public WeaponInventoryAdapter(Context context, int resource, List<Weapon> objects, List<Integer> weaponPictures) {
        super(context, resource, objects);
        this.weaponPictures = weaponPictures;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.weapon_inventory_cell, null);
        }

        if(v != null) {
            ImageView img_weapon = (ImageView) v.findViewById(R.id.img_weapon);
            TextView tv_weapon_name = (TextView) v.findViewById(R.id.tv_weapon_name);
            TextView tv_damage = (TextView) v.findViewById(R.id.tv_damage);
            TextView tv_weapon_description = (TextView) v.findViewById(R.id.tv_weapon_description);
            final ImageView img_upgrade = (ImageView) v.findViewById(R.id.img_upgrade);

            final Weapon weapon = getItem(position);
            img_weapon.setImageResource(weaponPictures.get(position));
            tv_weapon_name.setText(weapon.getName());
            tv_damage.setText(String.format("(%d ~ %d)", (int)weapon.getMinDamage(), (int)weapon.getMaxDamage()));
            tv_weapon_description.setText(weapon.getDescription());

            img_upgrade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(weapon.getLevel() < 5) {
                        LayoutInflater inflater = LayoutInflater.from(getContext());
                        final View alertDialogView = inflater.inflate(R.layout.dialog_weapon_upgrade, null);
                        final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();

                        alertDialog.setView(alertDialogView);

                        TextView tv_weapon_detail = (TextView) alertDialogView.findViewById(R.id.tv_weapon_detail);
                        TextView tv_current_damage = (TextView) alertDialogView.findViewById(R.id.tv_current_damage);
                        TextView tv_upgraded_damage = (TextView) alertDialogView.findViewById(R.id.tv_upgraded_damage);
                        TextView tv_player_coin = (TextView) alertDialogView.findViewById(R.id.tv_player_coin);
                        TextView tv_cost_of_upgrade = (TextView) alertDialogView.findViewById(R.id.tv_cost_of_upgrade);
                        TextView tv_player_coin_remaining = (TextView) alertDialogView.findViewById(R.id.tv_player_coin_remaining);

                        Button btn_upgrade = (Button) alertDialogView.findViewById(R.id.btn_upgrade);
                        Button btn_cancel = (Button) alertDialogView.findViewById(R.id.btn_cancel);

                        tv_weapon_detail.setText(String.format("%s (Level %d -> %d)", weapon.getName(), weapon.getLevel(), weapon.getLevel() + 1));
                        tv_current_damage.setText(String.format("%d ~ %d", (int)weapon.getMinDamage(), (int)weapon.getMaxDamage()));
                        tv_upgraded_damage.setText(String.format("%d ~ %d", (int)(weapon.getMinDamage() + weapon.getBonusDamagePerLevel()), (int)(weapon.getMaxDamage() + weapon.getBonusDamagePerLevel())));

                        final long player_coin = Storage.getInstance().getCoin();
                        final long cost_of_upgrade = Storage.getInstance().getWeaponUpgradeCost(weapon);
                        final long player_coin_remaining = player_coin - cost_of_upgrade;

                        if(player_coin_remaining < 0) {
                            btn_upgrade.setEnabled(false);
                        }

                        tv_player_coin.setText(player_coin + "");
                        tv_cost_of_upgrade.setText(cost_of_upgrade + "");
                        tv_player_coin_remaining.setText(player_coin_remaining + "");

                        btn_upgrade.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                weapon.setLevel(weapon.getLevel() + 1);
                                Storage.getInstance().saveLevel(Storage.getInstance().getWeaponLevel(), "weaponLevel");
                                Storage.getInstance().setCoin(player_coin_remaining);
                                if(weapon.getLevel() == 1) {
                                    Storage.getInstance().getAvailableWeapons().add(weapon);
                                }

                                alertDialog.dismiss();
                                notifyDataSetChanged();
                            }
                        });

                        btn_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });

                        alertDialog.show();
                    }

                    else {
                        img_upgrade.setEnabled(false);
                        img_upgrade.setAlpha(0.3f);
                    }

                    notifyDataSetChanged();
                }
            });

        }
        return v;
    }
}
