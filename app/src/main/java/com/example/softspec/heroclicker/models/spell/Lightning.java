package com.example.softspec.heroclicker.models.spell;

import android.os.AsyncTask;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.buff.Buff;
import com.example.softspec.heroclicker.models.buff.LightningBuff;
import com.example.softspec.heroclicker.models.calculator.DamageCalculator;

/**
 * Created by Earth on 28/5/2559.
 */
public class Lightning extends Spell {

    private static final String NAME = "Lightning";
    private static final String DESCRIPTION = "Give the chance to deal 200 damage for seconds.";
    private static final double DURATION = 5;
    private static final double BONUS_DAMAGE = 200;
    private static final double CHANCE_RATE = 0.2;
    private static final int RANDOM_TIME = 100;
    private static final double DECREASING_COOLDOWN_PER_LEVEL = 2;

    private Buff buff = new LightningBuff(getBonusDamage());

    public Lightning(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(final Player player, final Enemy enemy) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            long startTime = System.currentTimeMillis();

            @Override
            protected Void doInBackground(Void... params) {
                while ((System.currentTimeMillis() - startTime) / 1000.0 <= DURATION) {
                    double random = Math.random();
                    if (random <= CHANCE_RATE) {
                        System.out.println("LIGHTNING");
                        DamageCalculator.getInstance().addBuff(buff);
                    }

                    try {
                        Thread.sleep(RANDOM_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                return;
            }
        }.execute();
    }

    @Override
    public double getCooldown() {
        return super.getCooldown() - (DECREASING_COOLDOWN_PER_LEVEL * (getLevel()-1) );
    }

    public static double getBonusDamage() {
        return BONUS_DAMAGE;
    }
}

