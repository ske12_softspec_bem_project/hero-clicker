package com.example.softspec.heroclicker.models.command;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.Storage;
import com.example.softspec.heroclicker.models.calculator.DamageCalculator;

import java.util.Observable;

/**
 * Created by Earth on 28/5/2559.
 */
public class WeaponCommand extends Observable implements Command {
    private double damage;

    @Override
    public void execute(Player player, Enemy enemy) {
        damage = DamageCalculator.getInstance().calculate(player);
        enemy.setCurrentHP(enemy.getCurrentHP() - damage);
        Storage.getInstance().setCoin(Math.round(Storage.getInstance().getCoin() + (damage * Storage.getInstance().getEnemyReward().get(enemy) / enemy.getHP())));
        setChanged();
        notifyObservers(damage);
    }
}
