package com.example.softspec.heroclicker.models.spell;

import android.os.CountDownTimer;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.calculator.DamageCalculator;

/**
 * Created by Earth on 28/5/2559.
 */
public class TrueStrike extends Spell {

    private static final String NAME = "True Strike";
    private static final String DESCRIPTION ="Prevents your attacks from missing for a period of time.";
    private double duration = 5.0;
    private static final double INCREASING_DURATION_TIME_PER_LEVEL = 1;

    public TrueStrike(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(Player player, Enemy enemy) {
        System.out.println("True Strike !!");
        long startTime = System.currentTimeMillis();
        DamageCalculator.getInstance().setMissRate(0);

        CountDownTimer countDownTimer = new CountDownTimer((long) getActualDuration() * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //do nothing...
            }

            @Override
            public void onFinish() {
                DamageCalculator.getInstance().setMissRate(DamageCalculator.getInstance().getConstantMissRate());
            }
        }.start();
    }

    public double getActualDuration() {
        return duration + (INCREASING_DURATION_TIME_PER_LEVEL * (getLevel() - 1));
    }
}
