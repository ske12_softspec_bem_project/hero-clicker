package com.example.softspec.heroclicker.models;

/**
 * Created by Earth on 31/5/2559.
 */
public class Stage {

    private Enemy enemy;
    private float rating;

    public Stage(Enemy enemy) {
        this.enemy = enemy;
        rating = 0f;
    }

    public Enemy getEnemy() {
        return this.enemy;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
