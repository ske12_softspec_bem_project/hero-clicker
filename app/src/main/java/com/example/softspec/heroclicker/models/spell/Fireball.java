package com.example.softspec.heroclicker.models.spell;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.Storage;

/**
 * Created by Earth on 28/5/2559.
 */
public class Fireball extends Spell {

    private static final String NAME = "Fireball";
    private static final String DESCRIPTION ="Blasts an enemy with a wave of fire, dealing massive damage.";
    private static final double BASE_DAMAGE = 1000;

    public Fireball(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(Player player, Enemy enemy) {
        double damage = BASE_DAMAGE * getLevel();
        enemy.setCurrentHP(enemy.getCurrentHP() - damage);
        Storage.getInstance().setCoin(Math.round(Storage.getInstance().getCoin() + (damage * Storage.getInstance().getEnemyReward().get(enemy) / enemy.getHP())));
        setChanged();
        notifyObservers(damage);
    }
}
