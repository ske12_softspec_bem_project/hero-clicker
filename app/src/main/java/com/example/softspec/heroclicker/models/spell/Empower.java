package com.example.softspec.heroclicker.models.spell;

import android.os.AsyncTask;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;

/**
 * Created by Earth on 28/5/2559.
 */
public class Empower extends Spell {

    private static final String NAME = "Empower";
    private static final String DESCRIPTION ="Increases max damage of the weapon for 4 seconds.";
    private static final double BONUS_DAMAGE_PER_LEVEL = 20;
    private double damage = 100;
    private double duration = 4;

    public Empower(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(final Player player, Enemy enemy) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            long startTime = System.currentTimeMillis();

            @Override
            protected Void doInBackground(Void... params) {
                double recentMaxDamage = player.getWeapon().getMaxDamage();
                while((System.currentTimeMillis() - startTime) / 1000.0 <= getActualDuration() ) {
                    player.getWeapon().setMaxDamage(recentMaxDamage + getActualBonusDamage());
                }
                player.getWeapon().setMaxDamage(recentMaxDamage);
                return null;
            }
        }.execute();
    }

    public double getActualBonusDamage() {
        return damage + (BONUS_DAMAGE_PER_LEVEL * (getLevel() - 1));
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getActualDuration() {
        return duration + (getLevel() - 1);
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    @Override
    public double getCooldown() {
        return super.getCooldown() - (getLevel() - 1);
    }
}
