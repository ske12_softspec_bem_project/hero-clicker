package com.example.softspec.heroclicker.states;

import com.example.softspec.heroclicker.models.Game;

/**
 * Created by Earth on 30/5/2559.
 */
public interface State {
    void doAction(Game game);
    void stopAction();
}
