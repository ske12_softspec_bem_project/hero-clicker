package com.example.softspec.heroclicker.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.activities.MainActivity;
import com.example.softspec.heroclicker.activities.StageActivity;
import com.example.softspec.heroclicker.models.Stage;
import com.example.softspec.heroclicker.models.Storage;

import java.util.List;

/**
 * Created by Earth on 31/5/2559.
 */
public class StageAdapter extends ArrayAdapter<Stage> {

    public StageAdapter(Context context, int resource, List<Stage> stages) {
        super(context, resource, stages);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v =convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.stage_cell, null);
        }

        if( v != null) {
            ImageView img_enemy = (ImageView)v.findViewById(R.id.img_enemy);
            TextView stage_num = (TextView)v.findViewById(R.id.tv_stage);
            TextView enemy_name = (TextView)v.findViewById(R.id.tv_enemy_name);
            ImageButton btn_play = (ImageButton) v.findViewById(R.id.btn_play);
            RatingBar ratingBar = (RatingBar)v.findViewById(R.id.rate_enemy);

            final Stage stage = getItem(position);
            String stageName = String.format("Stage %d",position + 1);
            String[] img_enemy_state_resources = Storage.getInstance().getEnemyStateImageResources(Storage.getInstance().getEnemies().get(position));
            img_enemy.setImageResource(img_enemy.getContext().getResources().getIdentifier(img_enemy_state_resources[0], "drawable", img_enemy.getContext().getPackageName()));
            stage_num.setText(stageName);
            enemy_name.setText(stage.getEnemy().toString());

            ratingBar.setRating(stage.getRating());

            btn_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.putExtra("Enemy", stage.getEnemy().toString());
                    intent.putExtra("stage_position", position);
                    getContext().startActivity(intent);
                    ((StageActivity)getContext()).finish();
                }
            });
        }
        return v;
    }

}
