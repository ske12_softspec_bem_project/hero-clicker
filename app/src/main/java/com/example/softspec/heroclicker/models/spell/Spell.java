package com.example.softspec.heroclicker.models.spell;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;

import java.util.Observable;

/**
 * Created by Earth on 28/5/2559.
 */
public abstract class Spell extends Observable {
    private String name;
    private String description;
    private double cooldown;
    private int level;

    private boolean isOnCooldown;

    public Spell(String name, String description, double cooldown, int level) {
        this.name = name;
        this.description = description;
        this.cooldown = cooldown;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getCooldown() {
        return cooldown;
    }

    public void setCooldown(double cooldown) {
        this.cooldown = cooldown;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }

        if(!(o instanceof Spell)) {
            return false;
        }

        Spell spell = (Spell) o;

        return this.getName().equals(spell.getName());
    }

    public boolean isOnCooldown() {
        return isOnCooldown;
    }

    public void setOnCooldown(boolean onCooldown) {
        isOnCooldown = onCooldown;
    }

    public abstract void apply(Player player, Enemy enemy);
}
