package com.example.softspec.heroclicker.models;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.softspec.heroclicker.R;
import com.example.softspec.heroclicker.activities.StorageApplication;
import com.example.softspec.heroclicker.models.spell.AmplifyDamage;
import com.example.softspec.heroclicker.models.spell.Bloodrage;
import com.example.softspec.heroclicker.models.spell.Empower;
import com.example.softspec.heroclicker.models.spell.Fireball;
import com.example.softspec.heroclicker.models.spell.FurySwipes;
import com.example.softspec.heroclicker.models.spell.Lightning;
import com.example.softspec.heroclicker.models.spell.MortalStrike;
import com.example.softspec.heroclicker.models.spell.Spell;
import com.example.softspec.heroclicker.models.spell.TrueStrike;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Earth on 27/5/2559.
 */
public class Storage {
    private static Storage instance = null;

    private Player player;

    private List<Weapon> weapons;
    private List<Integer> weaponPictures;

    private List<Spell> spells;
    private List<Integer> spellPictures;

    private List<Enemy> enemies;

    private List<Stage> stages;

    private List<Integer> rewards;

    private long coin;

    private Map<Enemy, Integer> enemyReward;

    private int selectedWeaponPosition = 0;

    private List<Weapon> availableWeapons;

    private List<Spell> availableSpells;

    private int[] weaponLevel;

    private int[] spellLevel;

    private float[] stageRatings;

    private Map<Weapon, long[]> weaponUpgradeCost;

    private Map<Spell, long[]> spellUpgradeCost;

    private SharedPreferences sharedPreferences;

    private SharedPreferences.Editor editor;

    private Gson gson;

    private Storage() {
        weapons = new ArrayList<>();
        Weapon primalAxe = new Weapon("Primal Axe", "You grow stronger just by holding it.", 104, 204, 13, 1);
        Weapon maceOfRage = new Weapon("Mace of Rage", "A powerful mace that get the power of raging by a master warrior.", 124, 234, 13, 0);
        Weapon ironShard = new Weapon("Iron Shard", "A shard made by iron that can cuts through the enemy’s armor.", 144, 254, 13, 0);
        Weapon relicBlade = new Weapon("Relic Blade", "A blade was presented as a gift in hope of negotiating a peaceful surrender.", 164, 284, 13, 0);
        Weapon boneSabre = new Weapon("Bone Sabre", "A weapon of incredible power that is difficult to control.", 184, 304, 13, 0);
        Weapon mithrilBlade = new Weapon("Mithril Blade", "A blade made of only the most expensive crystals that can break the weakness of the enemy.", 210, 340, 16, 0);
        Weapon swordOfAscension = new Weapon("Sword of Ascension", "The lost sword for long time ago that cuts into an enemy's soul.", 240, 370, 20, 0);
        Weapon godRapier = new Weapon("God's Rapier", "A god weapon that often turns the tides of battle.", 270, 410, 20, 0);

        weapons.add(primalAxe);
        weapons.add(maceOfRage);
        weapons.add(ironShard);
        weapons.add(relicBlade);
        weapons.add(boneSabre);
        weapons.add(mithrilBlade);
        weapons.add(swordOfAscension);
        weapons.add(godRapier);

        weaponPictures = new ArrayList<>();
        weaponPictures.add(R.drawable.weapon_primal_axe);
        weaponPictures.add(R.drawable.weapon_mace_of_rage);
        weaponPictures.add(R.drawable.weapon_iron_shard);
        weaponPictures.add(R.drawable.weapon_relic_blade);
        weaponPictures.add(R.drawable.weapon_bone_sabre);
        weaponPictures.add(R.drawable.weapon_mithril_blade);
        weaponPictures.add(R.drawable.weapon_sword_of_ascension);
        weaponPictures.add(R.drawable.weapon_god_rapier);

        spells = new ArrayList<Spell>();

        Spell fireBall = new Fireball(10, 1);
        Spell empower = new Empower(25, 0);
        Spell lightning = new Lightning(20, 0);
        Spell bloodRage = new Bloodrage(20, 0);
        Spell truestrike = new TrueStrike(20, 0);
        Spell amplifyDamage = new AmplifyDamage(20, 0);
        Spell mortalStrike = new MortalStrike(20, 0);
        Spell furySwipes = new FurySwipes(25, 0);

        spells.add(fireBall);
        spells.add(empower);
        spells.add(lightning);
        spells.add(bloodRage);
        spells.add(truestrike);
        spells.add(amplifyDamage);
        spells.add(mortalStrike);
        spells.add(furySwipes);

        spellPictures = new ArrayList<>();
        spellPictures.add(R.drawable.spell_fireball);
        spellPictures.add(R.drawable.spell_empower);
        spellPictures.add(R.drawable.spell_lightning);
        spellPictures.add(R.drawable.spell_bloodrage);
        spellPictures.add(R.drawable.spell_true_strike);
        spellPictures.add(R.drawable.spell_amplify_damage);
        spellPictures.add(R.drawable.spell_mortal_strike);
        spellPictures.add(R.drawable.spell_fury_swipes);

        enemies = new ArrayList<>();

        Enemy originalElf = new Enemy.Builder("Elf").HP(30000).build();
        Enemy blackElf = new Enemy.Builder("Elf").HP(31000).color("Black").build();
        Enemy blondElf = new Enemy.Builder("Elf").HP(32000).color("Blond").build();
        Enemy blueElf = new Enemy.Builder("Elf").HP(33000).color("Blue").build();
        Enemy violetElf = new Enemy.Builder("Elf").HP(35000).color("Violet").build();

        Enemy originalSwordman = new Enemy.Builder("Swordman").HP(40000).build();
        Enemy graySwordman = new Enemy.Builder("Swordman").HP(42000).color("Gray").build();
        Enemy greenSwordman = new Enemy.Builder("Swordman").HP(44000).color("Green").build();
        Enemy pinkSwordman = new Enemy.Builder("Swordman").HP(46000).color("Pink").build();
        Enemy redSwordman = new Enemy.Builder("Swordman").HP(50000).color("Red").build();

        Enemy originalNinja = new Enemy.Builder("Ninja").HP(55000).build();
        Enemy blueNinja = new Enemy.Builder("Ninja").HP(60000).color("Blue").build();
        Enemy brownNinja = new Enemy.Builder("Ninja").HP(65000).color("Brown").build();
        Enemy greenNinja = new Enemy.Builder("Ninja").HP(70000).color("Green").build();
        Enemy violetNinja = new Enemy.Builder("Ninja").HP(75000).color("Violet").build();

        Enemy originalAxeman = new Enemy.Builder("Axeman").HP(80000).build();
        Enemy blondAxeman = new Enemy.Builder("Axeman").HP(90000).color("Blond").build();
        Enemy brownAxeman = new Enemy.Builder("Axeman").HP(100000).color("Brown").build();
        Enemy grayAxeman = new Enemy.Builder("Axeman").HP(110000).color("Gray").build();
        Enemy greenAxeman = new Enemy.Builder("Axeman").HP(120000).color("Green").build();

        Enemy originalKnight = new Enemy.Builder("Knight").HP(130000).build();
        Enemy blueKnight = new Enemy.Builder("Knight").HP(155000).color("Blue").build();
        Enemy greenKnight = new Enemy.Builder("Knight").HP(160000).color("Green").build();
        Enemy orangeKnight = new Enemy.Builder("Knight").HP(175000).color("Orange").build();
        Enemy purpleKnight = new Enemy.Builder("Knight").HP(200000).color("Purple").build();

        enemies.add(originalElf);
        enemies.add(blackElf);
        enemies.add(blondElf);
        enemies.add(blueElf);
        enemies.add(violetElf);

        enemies.add(originalSwordman);
        enemies.add(graySwordman);
        enemies.add(greenSwordman);
        enemies.add(pinkSwordman);
        enemies.add(redSwordman);

        enemies.add(originalNinja);
        enemies.add(blueNinja);
        enemies.add(brownNinja);
        enemies.add(greenNinja);
        enemies.add(violetNinja);

        enemies.add(originalAxeman);
        enemies.add(blondAxeman);
        enemies.add(brownAxeman);
        enemies.add(grayAxeman);
        enemies.add(greenAxeman);

        enemies.add(originalKnight);
        enemies.add(blueKnight);
        enemies.add(greenKnight);
        enemies.add(orangeKnight);
        enemies.add(purpleKnight);

        stages = new ArrayList<>();
        for (int i = 0; i < enemies.size(); i++) {
            stages.add(new Stage(enemies.get(i)));
        }

        int rewardsEnemy_1 = 1000;
        int rewardsEnemy_2 = 1200;
        int rewardsEnemy_3 = 1400;
        int rewardsEnemy_4 = 1600;
        int rewardsEnemy_5 = 2000;
        int rewardsEnemy_6 = 2500;
        int rewardsEnemy_7 = 3000;
        int rewardsEnemy_8 = 3500;
        int rewardsEnemy_9 = 4000;
        int rewardsEnemy_10 = 4500;
        int rewardsEnemy_11 = 5000;
        int rewardsEnemy_12 = 6000;
        int rewardsEnemy_13 = 7000;
        int rewardsEnemy_14 = 8000;
        int rewardsEnemy_15 = 10000;
        int rewardsEnemy_16 = 12000;
        int rewardsEnemy_17 = 13500;
        int rewardsEnemy_18 = 15000;
        int rewardsEnemy_19 = 16500;
        int rewardsEnemy_20 = 18000;
        int rewardsEnemy_21 = 20000;
        int rewardsEnemy_22 = 22000;
        int rewardsEnemy_23 = 24000;
        int rewardsEnemy_24 = 26000;
        int rewardsEnemy_25 = 30000;

        rewards = new ArrayList<>();
        rewards.add(rewardsEnemy_1);
        rewards.add(rewardsEnemy_2);
        rewards.add(rewardsEnemy_3);
        rewards.add(rewardsEnemy_4);
        rewards.add(rewardsEnemy_5);
        rewards.add(rewardsEnemy_6);
        rewards.add(rewardsEnemy_7);
        rewards.add(rewardsEnemy_8);
        rewards.add(rewardsEnemy_9);
        rewards.add(rewardsEnemy_10);
        rewards.add(rewardsEnemy_11);
        rewards.add(rewardsEnemy_12);
        rewards.add(rewardsEnemy_13);
        rewards.add(rewardsEnemy_14);
        rewards.add(rewardsEnemy_15);
        rewards.add(rewardsEnemy_16);
        rewards.add(rewardsEnemy_17);
        rewards.add(rewardsEnemy_18);
        rewards.add(rewardsEnemy_19);
        rewards.add(rewardsEnemy_20);
        rewards.add(rewardsEnemy_21);
        rewards.add(rewardsEnemy_22);
        rewards.add(rewardsEnemy_23);
        rewards.add(rewardsEnemy_24);
        rewards.add(rewardsEnemy_25);

        enemyReward = new HashMap<>();
        for(int i = 0; i < enemies.size(); i++) {
            enemyReward.put(enemies.get(i), rewards.get(i));
        }

        weaponUpgradeCost = new HashMap<>();
        long[] weapon_1_cost = new long[] {0, 1000, 2000, 3000, 4000};
        long[] weapon_2_cost = new long[] {5000, 2000, 3500, 5000, 5500};
        long[] weapon_3_cost = new long[] {10000, 4000, 7000, 10000, 13000};
        long[] weapon_4_cost = new long[] {20000, 5000, 10000, 15000, 20000};
        long[] weapon_5_cost = new long[] {50000, 10000, 20000, 30000, 40000};
        long[] weapon_6_cost = new long[] {80000 ,20000, 30000, 40000, 50000};
        long[] weapon_7_cost = new long[] {120000, 40000, 55000, 70000, 85000};
        long[] weapon_8_cost = new long[] {200000, 50000, 100000, 150000, 200000};

        weaponUpgradeCost.put(weapons.get(0), weapon_1_cost);
        weaponUpgradeCost.put(weapons.get(1), weapon_2_cost);
        weaponUpgradeCost.put(weapons.get(2), weapon_3_cost);
        weaponUpgradeCost.put(weapons.get(3), weapon_4_cost);
        weaponUpgradeCost.put(weapons.get(4), weapon_5_cost);
        weaponUpgradeCost.put(weapons.get(5), weapon_6_cost);
        weaponUpgradeCost.put(weapons.get(6), weapon_7_cost);
        weaponUpgradeCost.put(weapons.get(7), weapon_8_cost);

        spellUpgradeCost = new HashMap<>();
        long[] spell_1_cost = new long[] {0, 5000, 10000, 15000};
        long[] spell_2_cost = new long[] {20000, 30000, 40000, 50000};
        long[] spell_3_cost = new long[] {20000, 30000, 40000, 50000};
        long[] spell_4_cost = new long[] {25000, 35000, 45000, 55000};
        long[] spell_5_cost = new long[] {25000, 35000, 45000, 55000};
        long[] spell_6_cost = new long[] {25000, 35000, 45000, 55000};
        long[] spell_7_cost = new long[] {30000, 40000, 50000, 60000};
        long[] spell_8_cost = new long[] {25000, 35000, 45000, 55000};

        spellUpgradeCost.put(spells.get(0), spell_1_cost);
        spellUpgradeCost.put(spells.get(1), spell_2_cost);
        spellUpgradeCost.put(spells.get(2), spell_3_cost);
        spellUpgradeCost.put(spells.get(3), spell_4_cost);
        spellUpgradeCost.put(spells.get(4), spell_5_cost);
        spellUpgradeCost.put(spells.get(5), spell_6_cost);
        spellUpgradeCost.put(spells.get(6), spell_7_cost);
        spellUpgradeCost.put(spells.get(7), spell_8_cost);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(StorageApplication.getContext());
        editor = sharedPreferences.edit();
        gson = new Gson();

        String playerJSON = sharedPreferences.getString("PLAYER", "");
        if(playerJSON == "") {
            setPlayer(null);
        }

        else {
            Player player = gson.fromJson(playerJSON, Player.class);
            setPlayer(player);
        }

        long coin = sharedPreferences.getLong("COIN", 0);
        setCoin(coin);

        weaponLevel = new int[weapons.size()];
        spellLevel = new int[spells.size()];
        stageRatings = new float[stages.size()];

        weaponLevel = getLevel(weaponLevel, "weaponLevel");
        updateAllWeaponLevel();

        spellLevel = getLevel(spellLevel, "spellLevel");
        updateAllSpellLevel();

        stageRatings = getStageRatings(stageRatings, "stageRatings");
        updateAllStageRatings();

        availableWeapons = new ArrayList<>();
        availableSpells = new ArrayList<>();

        for(Weapon weapon : weapons) {
            if(weapon.getLevel() > 0) {
                availableWeapons.add(weapon);
            }
        }

        for(Spell spell : spells) {
            if (spell.getLevel() > 0) {
                availableSpells.add(spell);
            }
        }
    }

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }

        return instance;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
        String playerJSON = gson.toJson(player);
        editor.putString("PLAYER", playerJSON);
        editor.commit();
    }

    public List<Weapon> getWeapons() {
        return weapons;
    }

    public void setWeapons(List<Weapon> weapons) {
        this.weapons = weapons;
    }

    public Weapon getWeaponFromQuery(String query) {
        for(Weapon weapon : Storage.getInstance().getWeapons()) {
            if(weapon.toString().equals(query)) {
                return weapon;
            }
        }

        return null;
    }

    public List<Integer> getWeaponPictures() {
        return weaponPictures;
    }

    public void setWeaponPictures(List<Integer> weaponPictures) {
        this.weaponPictures = weaponPictures;
    }


    public List<Spell> getSpells() {
        return spells;
    }

    public void setSpells(List<Spell> spells) {
        this.spells = spells;
    }

    public List<Integer> getSpellPictures() {
        return spellPictures;
    }

    public void setSpellPictures(List<Integer> spellPictures) {
        this.spellPictures = spellPictures;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public Enemy getEnemyFromQuery(String query) {
        for(Enemy enemy : enemies) {
            if(enemy.toString().equalsIgnoreCase(query)) {
                return enemy;
            }
        }

        return null;
    }

    public int getSelectedWeaponPosition() {
        return selectedWeaponPosition;
    }

    public void setSelectedWeaponPosition(int selectedWeaponPosition) {
        this.selectedWeaponPosition = selectedWeaponPosition;
    }

    public void saveLevel(int[] array, String arrayName) {
        editor.putInt(arrayName + "_size", array.length);
        for(int i = 0; i < array.length; i++) {
            if(arrayName.equals("weaponLevel")) {
                editor.putInt(arrayName + "_" + i, weapons.get(i).getLevel());
            }

            else if(arrayName.equals("spellLevel")) {
                editor.putInt(arrayName + "_" + i, spells.get(i).getLevel());
            }
        }
        editor.commit();
    }

    public int[] getLevel(int[] level, String arrayName) {
        int size = sharedPreferences.getInt(arrayName + "_size", level.length);
        int[] array = new int[size];
        for(int i = 0; i < size; i++) {
            if(i == 0)  array[i] = sharedPreferences.getInt(arrayName + "_" + i, 1);
            else array[i] = sharedPreferences.getInt(arrayName + "_" + i, 0);
        }

        return array;
    }

    public void saveStageRatings(float[] array, String arrayName) {
        editor.putInt(arrayName + "_size", array.length);
        for(int i = 0; i < array.length; i++) {
            editor.putFloat(arrayName + "_" + i, stages.get(i).getRating());
        }

        editor.commit();
    }

    public float[] getStageRatings(float[] ratings, String arrayName) {
        int size = sharedPreferences.getInt(arrayName + "_size", ratings.length);
        float[] array = new float[size];
        for(int i = 0; i < size; i++) {
            array[i] = sharedPreferences.getFloat(arrayName + "_" + i, 0);
        }

        return array;
    }

    public float[] getStageRatings() {
        return stageRatings;
    }

    public int[] getWeaponLevel() {
        return weaponLevel;
    }

    public void updateAllWeaponLevel() {
        for(int i = 0; i < weapons.size(); i++) {
            weapons.get(i).setLevel(weaponLevel[i]);
        }
    }

    public int[] getSpellLevel() {
        return spellLevel;
    }

    public void updateAllSpellLevel() {
        for(int i = 0; i < spells.size(); i++) {
            spells.get(i).setLevel(spellLevel[i]);
        }
    }

    public void updateAllStageRatings() {
        for(int i = 0; i < stages.size(); i++) {
            stages.get(i).setRating(stageRatings[i]);
        }
    }

    public static void setInstance(Storage instance) {
        Storage.instance = instance;
    }

    public int getSpellContentPicture(Spell spell) {
        for(int i = 0; i < Storage.getInstance().getSpells().size(); i++) {
            if(spell != null && spell.equals(Storage.getInstance().getSpells().get(i))) {
                return Storage.getInstance().getSpellPictures().get(i);
            }
        }

        return R.drawable.spell_demo_green1;
    }

    public List<Stage> getStages() {
        return this.stages;
    }

    public String[] getEnemyStateImageResources(Enemy enemy) {
        String[] img_enemy_resources = new String[4];
        img_enemy_resources[0] = String.format("%s_%s_first_state", enemy.getName().toLowerCase(), enemy.getColor().toLowerCase());
        img_enemy_resources[1] = String.format("%s_%s_hit1_state", enemy.getName().toLowerCase(), enemy.getColor().toLowerCase());
        img_enemy_resources[2] = String.format("%s_%s_hit2_state", enemy.getName().toLowerCase(), enemy.getColor().toLowerCase());
        img_enemy_resources[3] = String.format("%s_%s_hit3_state", enemy.getName().toLowerCase(), enemy.getColor().toLowerCase());

        return img_enemy_resources;
    }

    public long getCoin() {
        return coin;
    }

    public void setCoin(long coin) {
        this.coin = coin;
        editor.putLong("COIN", coin);
        editor.commit();
    }

    public long getWeaponUpgradeCost(Weapon weapon) {
        return weaponUpgradeCost.get(weapon)[weapon.getLevel()];
    }

    public long getSpellUpgradeCost(Spell spell) {
        return spellUpgradeCost.get(spell)[spell.getLevel()];
    }

    public Map<Enemy, Integer> getEnemyReward() {
        return enemyReward;
    }

    public void setEnemyReward(Map<Enemy, Integer> enemyReward) {
        this.enemyReward = enemyReward;
    }

    public List<Weapon> getAvailableWeapons() {
        return availableWeapons;
    }

    public List<Spell> getAvailableSpells() {
        return availableSpells;
    }
}
