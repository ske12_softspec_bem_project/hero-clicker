package com.example.softspec.heroclicker.models.spell;

import android.os.AsyncTask;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.buff.AmplifyDamageBuff;
import com.example.softspec.heroclicker.models.buff.Buff;
import com.example.softspec.heroclicker.models.calculator.DamageCalculator;

/**
 * Created by Earth on 28/5/2559.
 */
public class AmplifyDamage extends Spell {

    private static final String NAME = "Amplify Damage";
    private static final String DESCRIPTION ="Amplify damage for each attack for 6 seconds.";
    private static final double DURATION = 6.0;
    private double bonusDamageRate = 0.15;
    private static final double BONUS_DAMAGE_RATE_PER_LEVEL = 0.05;

    private Buff buff = new AmplifyDamageBuff(getActualBonusDamageRate());


    public AmplifyDamage(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(Player player, Enemy enemy) {
        System.out.println(getName());
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            long startTime = System.currentTimeMillis();

            @Override
            protected Void doInBackground(Void... params) {
                while((System.currentTimeMillis() - startTime) / 1000.0 <= DURATION ) {
                    DamageCalculator.getInstance().addBuff(buff);
                }
                System.out.println(getName() + " disabled");
                return null;
            }
        }.execute();
    }

    public double getActualBonusDamageRate() {
        return bonusDamageRate + (BONUS_DAMAGE_RATE_PER_LEVEL * (getLevel() - 1));
    }

    public void setBonusDamageRate(double bonusDamageRate) {
        this.bonusDamageRate = bonusDamageRate;
    }
}
