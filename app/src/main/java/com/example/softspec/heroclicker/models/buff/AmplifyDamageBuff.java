package com.example.softspec.heroclicker.models.buff;

/**
 * Created by Earth on 28/5/2559.
 */
public class AmplifyDamageBuff implements Buff {

    private double bonusDamageRate;

    public AmplifyDamageBuff(double bonusDamageRate) {
        this.bonusDamageRate = bonusDamageRate;
    }

    @Override
    public double affect(double damage) {
        return bonusDamageRate*damage;
    }
}
