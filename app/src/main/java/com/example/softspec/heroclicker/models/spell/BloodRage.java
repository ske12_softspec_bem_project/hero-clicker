package com.example.softspec.heroclicker.models.spell;

import com.example.softspec.heroclicker.models.Enemy;
import com.example.softspec.heroclicker.models.Player;
import com.example.softspec.heroclicker.models.buff.BloodrageBuff;
import com.example.softspec.heroclicker.models.buff.Buff;
import com.example.softspec.heroclicker.models.calculator.DamageCalculator;

/**
 * Created by Earth on 28/5/2559.
 */
public class Bloodrage extends Spell {

    private static final String NAME = "Bloodrage";
    private static final String DESCRIPTION ="Give the chance for a single attack to deal massive damage.";
    private static final double CHANCE_RATE = 0.7;
    private static final double BONUS_DAMAGE_PER_LEVEL = 0.2;
    private static final double DECREASING_COOLDOWN_PER_LEVEL = 1;

    private double bonusDamage = 2.4;

    private Buff buff = new BloodrageBuff(getActualBonusDamage());

    public Bloodrage(double cooldown, int level) {
        super(NAME, DESCRIPTION, cooldown, level);
    }

    @Override
    public void apply(Player player, Enemy enemy) {
        double random = Math.random();
        if(random <= CHANCE_RATE) {
            System.out.println("BLOODRAGE !!");
            DamageCalculator.getInstance().addBuff(buff);
        }
    }

    public double getActualBonusDamage() {
        return bonusDamage + (BONUS_DAMAGE_PER_LEVEL * (getLevel() - 1) );
    }

    public void setDamage(double bonusDamage) {
        this.bonusDamage = bonusDamage;
    }

    @Override
    public double getCooldown() {
        return super.getCooldown() - (DECREASING_COOLDOWN_PER_LEVEL * (getLevel() - 1));
    }
}
